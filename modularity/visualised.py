import networkx as nx
import networkx.algorithms.community as nxcom
from matplotlib import pyplot as plt
from matplotlib import cm


def visualised(graph, communities):
    """
    This function can be used to visualise the graph by colouring the nodes in a community
    :param graph:
    :param communities:
    """
    # Count the communities
    print(f"The graph has {len(communities)} communities.")
    # Set node and edge communities
    set_node_community(graph, communities)
    set_edge_community(graph)
    node_color = [get_color(graph.nodes[v]['community']) for v in graph.nodes]
    # Set community color for edges between members of the same community (internal)
    # and intra-community edges (external)
    external = [(v, w) for v, w in graph.edges if graph.edges[v, w]['community'] == 0]
    internal = [(v, w) for v, w in graph.edges if graph.edges[v, w]['community'] > 0]
    internal_color = ['black' for e in internal]
    graph_pos = nx.spring_layout(graph)
    plt.rcParams.update({'figure.figsize': (15, 10)})
    # Draw external edges
    nx.draw_networkx(
        graph,
        pos=graph_pos,
        node_size=0,
        edgelist=external,
        edge_color="silver")
    # Draw nodes and internal edges
    nx.draw_networkx(
        graph,
        pos=graph_pos,
        node_color=node_color,
        edgelist=internal,
        edge_color=internal_color)
    print(communities)
    print(nxcom.modularity(graph, communities))
    plt.show()


def set_node_community(g, communities):
    """Add community to node attributes"""
    for c, v_c in enumerate(communities):
        for v in v_c:
            # Add 1 to save 0 for external edges
            g.nodes[v]['community'] = c + 1


def set_edge_community(g):
    """Find internal edges and add their community to their attributes"""
    for v, w, in g.edges:
        if g.nodes[v]['community'] == g.nodes[w]['community']:
            # Internal edge, mark with community
            g.edges[v, w]['community'] = g.nodes[v]['community']
        else:
            # External edge, mark as 0
            g.edges[v, w]['community'] = 0


def get_color(i, r_off=1, g_off=1, b_off=1):
    """Assign a color to a vertex."""
    n = 16
    low, high = 0.1, 0.9
    span = high - low
    r = low + span * (((i + r_off) * 3) % n) / (n - 1)
    g = low + span * (((i + g_off) * 5) % n) / (n - 1)
    b = low + span * (((i + b_off) * 7) % n) / (n - 1)
    return r, g, b


# alternative
def visualised_alt(graph):
    communities = nxcom.modularity(graph)
    pos = nx.spring_layout(graph)
    # color the nodes according to their partition
    cmap = cm.get_cmap('viridis', max(communities.values()) + 1)
    nx.draw_networkx_nodes(graph, pos, communities.keys(), node_size=40,
                        cmap=cmap, node_color=list(communities.values()))
    nx.draw_networkx_edges(graph, pos, alpha=0.5)
    plt.show()