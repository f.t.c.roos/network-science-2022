import networkx.algorithms.community as nxcom


def louvain(graph):
    return modularity(graph)


def modularity(graph):
    communities = nxcom.louvain_communities(graph)
    return nxcom.modularity(graph, communities)

