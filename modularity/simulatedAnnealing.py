import copy
import math
import random
import time
import networkx.algorithms.community as nxcom


def main(graph, t=0.1, c=0.9997, ft=0.01):
    """ The goal is to maximise the modularity. """
    if len(graph.nodes) <= 1:
        return 0

    # We experimented with the parameters, but first we calculated that t = 0.072 gives a 50% change for an average
    # start negative score (-0.05) to be accepted and that approximately 0.005 gives a 10% change for a small end
    # negative score (-0.01) to be accepted. Further parameters tweaking might improve the algorithm.

    # We made the temperatures a little bit dynamic on the graph size
    temperature = t + graph.number_of_nodes() / 100000
    final_temp = max(0.000001, ft - graph.number_of_nodes() / 1000000)
    cooling = c

    # Start by initializing the start state:

    # - there multiple possible initial solutions for the k-community case:
    # 1. all nodes can be in singleton communities
    # 2. all nodes can be in one big community
    # 3. all nodes will be randomly spread over communities of size of the average degree

    # for the 2-community case, we start with a start solution of 2 communities of approxi even side by random placement
    current_communities = [set(), set()]
    for node in graph.nodes:
        if random.random() >= 0.5:
            current_communities[0].add(node)
        else:
            current_communities[1].add(node)

    # Calculates modularity of the start solution
    current_cost = nxcom.modularity(graph, current_communities)
    # best solution so far
    best_cost = current_cost

    # We won't calculate the entire modularity again from this point, only the delta modularity.
    # We need the store some information about the states to calculate the delta modularity and update the information.
    # The delta modularity was proposed by Blondel and collaborators: Fast unfolding of communities in large networks.

    # We will store per node how many neighbors are in the other community
    current_k_i_ins = []
    for i in range(0, len(graph.nodes)):
        ki_in = 0
        set_of_i = 0
        if str(i) in current_communities[1]:
            set_of_i = 1

        for neighbor in graph.neighbors(str(i)):
            if neighbor not in current_communities[set_of_i]:
                ki_in += 1
        current_k_i_ins.append(ki_in)

    # We will store per community the edges inside the component,
    current_sum_in = []
    # and store per community the total number of edges of all nodes in the component, so including the outgoing edges.
    current_sum_tot = []
    for i in [0, 1]:
        sum_tot = 0
        sum_in = 0
        for node in current_communities[i]:
            sum_tot += graph.degree[node]
            for neighbor in graph.neighbors(node):
                if neighbor in current_communities[i]:
                    sum_in += 1
        current_sum_in.append(sum_in)
        current_sum_tot.append(sum_tot)

    while temperature > final_temp:
        neighbor_state, delta_modularity, neighbor_sum_in, neighbor_sum_tot, i, c = get_neighbor(
            graph,
            copy.deepcopy(current_communities),
            current_k_i_ins,
            copy.deepcopy(current_sum_in),
            copy.deepcopy(current_sum_tot)
        )

        # if the new solution is better, accept it
        if delta_modularity >= 0:
            current_communities = neighbor_state
            current_cost += delta_modularity
            # The k_i_in's are updated after the new solution is accepted for efficiency
            current_k_i_ins = update_k_i_ins(graph, current_communities, copy.deepcopy(current_k_i_ins), i, c)
            current_sum_in = neighbor_sum_in
            current_sum_tot = neighbor_sum_tot
            # Check if new current_communities is the best so far
            if current_cost > best_cost:
                best_cost = current_cost
        # if the new solution is not better, accept it with a probability of e^(delta_modularity/temp)
        elif random.random() < math.exp(delta_modularity / temperature):
            current_communities = neighbor_state
            current_cost += delta_modularity
            # The k_i_in's are updated after the new solution is accepted for efficiency
            current_k_i_ins = update_k_i_ins(graph, current_communities, copy.deepcopy(current_k_i_ins), i, c)
            current_sum_in = neighbor_sum_in
            current_sum_tot = neighbor_sum_tot

        # cooling the temperature
        temperature *= cooling

    return best_cost


def get_neighbor(graph, communities, ki_ins, sum_in, sum_tot):
    """
    Gets a neighbor state by randomly moving a node to the other community.
    It returns the new state, the updated sum_in and sum_tot, and the moved node i and community c for update_k_i_ins()
    """
    # c is the component the node is moving to & d is the other component the node is moving from
    # We move a random node to a community if one is empty, or else we will pick a random node and community
    if len(communities[0]) == 0:
        c = 0
        d = 1
    elif len(communities[1]) == 0 or random.random() >= (len(communities[1]) / graph.number_of_nodes()):
        c = 1
        d = 0
    else:
        c = 0
        d = 1

    # The pop() method removes a random item from the set.
    i = communities[d].pop()
    communities[c].add(i)

    m2 = len(graph.edges) * 2

    # delta modularity of moving node i to component c
    q_after = ((sum_in[c] + (2 * ki_ins[int(i)])) / m2 - math.pow((sum_tot[c] + graph.degree[i]) / m2, 2)) - \
              (sum_in[c] / m2 - math.pow(sum_tot[c] / m2, 2) - math.pow((graph.degree[i]) / m2, 2))

    # we update the total sum of the edges in and from the communities
    if c == 0:
        new_sum_tot = [sum_tot[0] + graph.degree[i], sum_tot[1] - graph.degree[i]]
    else:
        new_sum_tot = [sum_tot[0] - graph.degree[i], sum_tot[1] + graph.degree[i]]

    # we update the total sum of the edges inside the communities
    new_sum_in = [sum_in[0], sum_in[1]]
    for neighbour in graph.neighbors(str(i)):
        if neighbour in communities[d]:
            new_sum_in[d] -= 2
        else:
            new_sum_in[c] += 2

    # delta modularity of moving node i out of component d
    q_before = ((new_sum_in[d] + (2 * (graph.degree[i] - ki_ins[int(i)]))) / m2 -
                math.pow((new_sum_tot[d] + graph.degree[i]) / m2, 2)) - \
               (new_sum_in[d] / m2 - math.pow(new_sum_tot[d] / m2, 2) -
                math.pow((graph.degree[i]) / m2, 2))

    # delta modularity = delta modularity of moving node i to component c - delta modularity of moving node i out of d
    return communities, q_after - q_before, new_sum_in, new_sum_tot, i, c


def update_k_i_ins(graph, communities, new_k_i_ins, i, c):
    """
    This function could be a part of the node moving function and updating the information,
    but it is pulled out of the node moving function function because of efficiency reasons.
    We update the k_i_in's by adding/subtracting the degree of the moving node based on its previous/new community.
    i : the node that has moved
    c : the community the node has moved to
     """
    # We first update the k_i_in of the node that has moved.
    new_k_i_ins[int(i)] = graph.degree[i] - new_k_i_ins[int(i)]

    # We then update the k_i_in's of the neighbors of the moved node, the k_i_in's of the other nodes are not affected.
    for neighbour in graph.neighbors(i):
        # We will check per neighbor if it is now in the same community, resulting in adding or subtracting one.
        if neighbour in communities[c]:
            new_k_i_ins[int(neighbour)] -= 1
        else:
            new_k_i_ins[int(neighbour)] += 1

    return new_k_i_ins


def optimise_parameters(graph):
    """
    This function is used to optimise the parameters of the simulated annealing algorithm by experimenting.
    """
    progress_counter = 0
    lines = []
    for t in [0.1]:
        lines.append("t = " + str(t))
        for c in [0.9997]:
            lines.append("  c = " + str(c))
            for ft in [0.01]:
                lines.append("      ft = " + str(ft))
                sum_score = 0
                sum_time = 0
                loops = 15
                for i in range(loops):
                    start_time = time.time()
                    sum_score += main(graph, t, c, ft)
                    stop_time = time.time()
                    sum_time += (stop_time - start_time)
                lines.append("          <score>: " + str(round((sum_score / loops), 2)))
                lines.append("          <time >: " + str(round((sum_time / loops), 2)))
                progress_counter += 1
                print(progress_counter / 2)

    filename = "optimise_parameters.txt"
    f = open(filename, "w")
    f.writelines("%s\n" % l for l in lines)
