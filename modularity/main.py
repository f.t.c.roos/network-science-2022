import warnings
import networkx as nx
import clausetNewmanMoore as cnm
import simulatedAnnealing as sa
import louvain as lv
import nodeMoving as nm
import time
import csv
warnings.simplefilter(action='ignore', category=FutureWarning)
import spectral_decomposition as sd


def test():
    """
    This function is used for testing the algorithms. There are two folders with graphs, one contains example graphs of
    the seven graph generators and the other one contain the 7777 experiment graphs.
    """
    print("     Algorithms          Modularity")
    graph = nx.read_edgelist(".\..\graphGenerator\exampleGraphs\connectedCaveman.edgelist")

    print("Clauset Newman Moore:      " + str(cnm.clauset_newman_moore(graph)))
    print("Louvain:                   " + str(lv.louvain(graph)))
    print("Spectral decomposition:    " + str(sd.spectral_decomposition(graph)))
    print("Node moving:               " + str(nm.node_moving(graph)))
    print("Simulated annealing:       " + str(sa.main(graph)))

    print("END")


def main():
    """
    This function is used to run the algorithms on the graphs. Obtaining the results of the 7777 graphs took us
    approximately 40 hours. The results are appended on the result csv files, so for testing I propose you can pick a
    range of 7.
    """
    print("START")

    # header of CSV files
    lines_results_run_time = ["Clauset Newman Moore,Louvain,Spectral decomposition,Node moving,Simulated annealing"]
    lines_results_modularity = ["Clauset Newman Moore,Louvain,Spectral decomposition,Node moving,Simulated annealing"]
    lines_results = ["n,m,<k>,second moment,third moment,C,<C>,d_max,<d>,<g>,r,D,algorithm"]
    lines_results_score = ["Clauset Newman Moore,Louvain,Spectral decomposition,Node moving,Simulated annealing"]

    # t_max : highest measured runtime over all tests
    t_max = 30
    # labeling_function = modularity + (1 - t/t_max)

    for i in range(7777):
        graph = nx.read_edgelist(".\..\graphGenerator\graphs\\" + str(i) + ".edgelist")

        print(i)

        # print(i, 1)
        # Clauset Newman Moore
        start_time = time.time()
        mod_cnm = cnm.clauset_newman_moore(graph)
        time_cnm = time.time() - start_time
        score_cnm = mod_cnm + (1 - time_cnm / t_max)

        score_best = score_cnm
        best = "Clauset Newman Moore"

        # print(i, 2)
        # Louvain
        start_time = time.time()
        mod_lv = lv.louvain(graph)
        time_lv = time.time() - start_time
        score_lv = mod_lv + (1 - time_lv / t_max)

        if score_lv > score_best:
            score_best = score_lv
            best = "Louvain"

        # print(i, 3)
        # Spectral decomposition
        start_time = time.time()
        mod_sd = sd.spectral_decomposition(graph)
        time_sd = time.time() - start_time
        score_sd = mod_sd + (1 - time_sd / t_max)

        if score_sd > score_best:
            score_best = score_sd
            best = "Spectral decomposition"

        # print(i, 4)
        # Node moving
        start_time = time.time()
        mod_nm = nm.node_moving(graph)
        time_nm = time.time() - start_time
        score_nm = mod_nm + (1 - time_nm / t_max)

        if score_nm > score_best:
            score_best = score_nm
            best = "Node moving"

        # print(i, 5)
        # Simulated annealing
        start_time = time.time()
        mod_sa = sa.main(graph)
        time_sa = time.time() - start_time
        score_sa = mod_sa + (1 - time_sa / t_max)

        if score_sa > score_best:
            # score_best = score_sa
            best = "Simulated annealing"

        f = open(".\..\graphGenerator\graphs\\" + str(i) + ".properties", newline='')
        csv_reader = csv.reader(f)
        properties = next(csv_reader)

        lines_results_run_time.append(str(time_cnm) + "," + str(time_lv) + "," + str(time_sd) + ","
                                      + str(time_nm) + "," + str(time_sa))
        lines_results_modularity.append(str(mod_cnm) + "," + str(mod_lv) + "," + str(mod_sd) + ","
                                        + str(mod_nm) + "," + str(mod_sa))
        lines_results.append(properties[0] + "," + properties[1] + "," + properties[2] + "," + properties[3] + ","
                             + properties[4] + "," + properties[5] + "," + properties[6] + "," + properties[7] + ","
                             + properties[8] + "," + properties[9] + "," + properties[10] + "," + properties[11] + ","
                             + best)
        lines_results_score.append(str(score_cnm) + "," + str(score_lv) + "," + str(score_sd) + ","
                                   + str(score_nm) + "," + str(score_sa))

    f_results_run_time = open("resultsRunTime.csv", "a")
    f_results_run_time.writelines("%s\n" % l for l in lines_results_run_time)
    f_results_modularity = open("resultsModularity.csv", "a")
    f_results_modularity.writelines("%s\n" % l for l in lines_results_modularity)
    f_results = open("results.csv", "a")
    f_results.writelines("%s\n" % l for l in lines_results)
    f_results_score = open("resultsScore.csv", "a")
    f_results_score.writelines("%s\n" % l for l in lines_results_score)
    print("END")


# run the experiment and obtain the results
# main()

# run a test
test()
