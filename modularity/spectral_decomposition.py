import modularity_maximization.community_newman as newman


def spectral_decomposition(graph):
    return modularity(graph)


def modularity(graph):
    return newman.partition(graph)
