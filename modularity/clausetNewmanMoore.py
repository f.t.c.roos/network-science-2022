import networkx.algorithms.community as nxcom


def clauset_newman_moore(graph):
    return modularity(graph)


def modularity(graph):
    # https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.community.modularity_max.greedy_modularity_communities.html
    # Find the communities
    communities = nxcom.greedy_modularity_communities(G=graph, cutoff=2, best_n=2)
    # return modularity
    return nxcom.modularity(graph, communities)
