import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

# initialize data
data = pd.read_csv('Analysis/results.csv')
data = data[['n','m','<k>','second moment','third moment','C','<C>','d_max','<d>','<g>','r','D','algorithm']]

# split into two sets
training_set, test_set = train_test_split(data, test_size = 0.2, random_state = 21)

# clean data
le = LabelEncoder()
data['algorithm'] = le.fit_transform(data['algorithm'])

X_train = training_set.iloc[:,0:-1].values
Y_train = training_set.iloc[:,-1].values
X_test = test_set.iloc[:,0:-1].values
Y_test = test_set.iloc[:,-1].values

# initialize classifier
classifier = MLPClassifier(hidden_layer_sizes=(200,100,50), max_iter = 300, activation = 'logistic', solver='adam', random_state=1)
classifier.fit(X_train, Y_train)

# predict test set
Y_pred = classifier.predict(X_test)

# calc accuracy
def accuracy(confusion_matrix):
   diagonal_sum = confusion_matrix.trace()
   sum_of_all_elements = confusion_matrix.sum()
   return diagonal_sum / sum_of_all_elements

cm = confusion_matrix(Y_pred, Y_test)
#print(cm)
print(accuracy(cm))
