import networkx.algorithms.community as nxcom


def node_moving(graph):
    return modularity(graph)


# implementation based on Newman
def modularity(graph):
    communities = nxcom.kernighan_lin_bisection(graph)
    return nxcom.modularity(graph, communities)
