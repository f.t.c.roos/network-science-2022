import pandas as pd

# calculate all avg runtimes

headers = ['Clauset Newman Moore','Louvain','Spectral decomposition','Node moving','Simulated annealing']
dfRt = pd.read_csv('Analysis/resultsRuntime.csv')
dfMod = pd.read_csv('Analysis/resultsModularity.csv')
dfScore = pd.read_csv('Analysis/resultsScore.csv')

print("RUNTIME")
for alg in headers:
    print("The average runtime of " + alg + " is:", dfRt[alg].mean())

print("MODULARITY")
for alg in headers:
    print("The average modularity of " + alg + " is:", dfMod[alg].mean())

print("SCORE")
for alg in headers:
    print("The average score of " + alg + " is:", dfMod[alg].mean())