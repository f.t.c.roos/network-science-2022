import glob, os
import scipy.stats as ss
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# plot hist of how frequencies of chosen algorithms

headers = ['algorithm']
df = pd.read_csv('Analysis/results.csv', names=headers)

print(df['algorithm'].value_counts())