import csv

"""
This script calculates the average runtime, modularity and score and prints it
"""

# open files, read headers and do nothing
csv_results_reader = csv.reader(open("Analysis/results.csv"), delimiter=',')
next(csv_results_reader)

csv_results_runtime_reader = csv.reader(open("Analysis/resultsRunTime.csv"), delimiter=',')
next(csv_results_runtime_reader)

csv_results_modularity_reader = csv.reader(open("Analysis/resultsModularity.csv"), delimiter=',')
next(csv_results_modularity_reader)

csv_results_score_reader = csv.reader(open("Analysis/resultsScore.csv"), delimiter=',')
next(csv_results_score_reader)

# calculate avg runtime and modularity for classifier selected algorithms / all algorithms separately

sum_runtime_classifier = 0
sum_modularity_classifier = 0
sum_score_classifier = 0

sum_runtime_NM = 0
sum_modularity_NM = 0
sum_score_NM = 0

# Number of results
n = 7777

for i in range(n):
    row_results = next(csv_results_reader)
    row_results_runtime = next(csv_results_runtime_reader)
    row_results_modularity = next(csv_results_modularity_reader)
    row_results_score = next(csv_results_score_reader)

    sum_runtime_NM += float(row_results_runtime[3])
    sum_modularity_NM += float(row_results_modularity[3])
    sum_score_NM += float(row_results_score[3])

    if row_results[12] == 'Clauset Newman Moore':
        sum_runtime_classifier += float(row_results_runtime[0])
        sum_modularity_classifier += float(row_results_modularity[0])
        sum_score_classifier += float(row_results_score[0])
    elif row_results[12] == 'Louvain':
        sum_runtime_classifier += float(row_results_runtime[1])
        sum_modularity_classifier += float(row_results_modularity[1])
        sum_score_classifier += float(row_results_score[1])
    elif row_results[12] == 'Spectral decomposition':
        sum_runtime_classifier += float(row_results_runtime[2])
        sum_modularity_classifier += float(row_results_modularity[2])
        sum_score_classifier += float(row_results_score[2])
    elif row_results[12] == 'Node moving':
        sum_runtime_classifier += float(row_results_runtime[3])
        sum_modularity_classifier += float(row_results_modularity[3])
        sum_score_classifier += float(row_results_score[3])
    elif row_results[12] == 'Simulated Annealing':
        sum_runtime_classifier += float(row_results_runtime[4])
        sum_modularity_classifier += float(row_results_modularity[4])
        sum_score_classifier += float(row_results_score[4])


average_runtime = sum_runtime_classifier / n
print(f'The average runtime of the classifier selection algorithm is   : {average_runtime}')
average_runtime = sum_runtime_NM / n
print(f'The average runtime of node moving is       : {average_runtime}')

average_modularity = sum_modularity_classifier / n
print(f'The average modularity of the classifier selection algorithm is: {average_modularity}')
average_modularity = sum_modularity_NM / n
print(f'The average modularity of node moving is    : {average_modularity}')

average_runtime = sum_runtime_NM / n
print(f'The average runtime of node moving  is      : {average_runtime}')

average_score = sum_score_classifier / n
print(f'The average score of the classifier selection algorithm is     : {average_score}')
average_score = sum_score_NM / n
print(f'The average score of node moving is         : {average_score}')

