import glob
import os
import scipy.stats as ss
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# this class contains functions for analyzing the generated graphs
# that is, plot the variation of the considered properties.

# merging all property files into one
def generate_data_file():
    with open("Analysis/properties.csv", "w") as f:
        for file in glob.glob("graphGenerator/graphs/*.properties"):
            with open(file, "r") as g:
                line = g.read() + '\n'
                f.write(line)


# create histograms of the properties over all graphs
def plot():
    # setting up the axes
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    # now plot
    alpha, loc, beta = 5, 100, 22

    headers = ['n', 'm', '<k>', 'second moment', 'third moment', 'C', '<C>', 'd_max', '<d>', '<g>', 'r', 'D']
    df = pd.read_csv('Analysis/properties.csv', names=headers)

    for col in headers:
        hist = df[col].plot(kind='hist', title = col)
        fig = ax.get_figure()
        fig.savefig('Analysis/plots/' + col.replace("<", "").replace(">", "_avg") + '.png')
        fig.clear()


# first generate the data file, then plot.
# generate_data_file()
# plot()
