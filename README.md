# Algorithm Selection for Community Detection using Classification
Creating a classifier to choose the best modularity seeking algorithm on graphs.

For full documentation about the research, we refer to paper.

## Table of contents
1. Graph generator
2. Graphs
3. Community detection algorithms
4. Experiments on modularity and runtime
5. Classifier
6. Analysis

## 1. Graph generator
The graph generator can be found in the [graphGenerator](graphGenerator/) folder.

The folder contains a python script called [main.py](graphGenerator/main.py). The script creates a set of graphs via seven community graph generators, imported from [NetworkX](https://networkx.org/documentation/stable/reference/generators.html#module-networkx.generators.community).   

## 2. Graphs
The set of 7777 graphs that is created via the seven graph generators can be found in the [graphs](graphGenerator/graphs) folder. One graph consist of a edgelist file and a property file. The edgelist is transformed into a graph during the experiments and the properties are then used to create the data for the classifier.

We also present seven example graphs by the seven graph generators. These can be found in the [exampleGraphs](graphGenerator/exampleGraphs) folder. Here we added, besides a edgelist and property file, also png of the graphs.

## 3. Community detection algorithms
The community detection algorithms can be found in the [modularity](modularity/) folder. The [main.py](modularity/main.py) file is the base script. This file refers to the 5 algorithms: [Clauset-Newman-Moore](modularity/clausetNewmanMoore.py), [Louvain](modularity/louvain.py), [simple node moving](modularity/nodeMoving.py), [Simulated Annealing](modularity/simulatedAnnealing.py) and [Spectral decomposition](modularity/spectral_decomposition.py). The test() function can be used to test the 5 algorithms on a graph. Three of the five are imported from the NetworkX library 

## 4. Experiments on modularity and runtime
The experiments are done via the main() function in [main.py](modularity/main.py). The function creates a graph from an edgelist file and runs the five algorithms over the graph. It records the runtime, the returned modularity, and computes a score. The results are then stored in csv files as data for the classifier and for analysis. The training data for the classifier can be found at [results.csv](modularity/results.csv).

## 5. Classifier
The classifier can be found at [classifier.py](modularity/classifier.py).

## 6. Analysis
The analysis of the graphs, experiments and classifier is done in the [Analysis](Analysis/) folder.

----------
Creators: Floris Roos (6240992) and Jos Zuijderwijk (5677130).
Utrecht University, Network Science, 24/6/2022
