import networkx as nx
import random as rd


def main():
    """
    function to generate a set of graphs. Every iteration return 7 graphs, so 1111 should return 7777 graphs.
    Generating the set of 1111 graphs takes approximately 1 min.
    """
    print("Start")
    for i in range(571, 1111):
        print(i)

        # max network size is 200

        # 1. Returns a connected caveman graph of l cliques of size k.
        k = rd.randint(3, 100)  # size of cliques (k at least 2 or NetworkXError is raised)
        g = nx.connected_caveman_graph(2, k)
        is_connected = nx.is_connected(g)
        while not is_connected:
            g = nx.connected_caveman_graph(2, k)
            is_connected = nx.is_connected(g)
        number = i * 7  # + 0
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

        # 2. Generate a Gaussian random partition graph.
        n = rd.randint(2, 200)
        p_in = rd.uniform(0.15, 0.35)
        p_out = rd.uniform(0.05, 0.15)
        variance = rd.randint(1, min(n, 15))
        g = nx.gaussian_random_partition_graph(n, variance, variance, p_in, p_out)
        is_connected = nx.is_connected(g)
        while not is_connected:
            p_in = rd.uniform(0.1, 0.5)
            p_out = rd.uniform(0.05, 0.20)
            variance = rd.randint(1, min(n, 15))
            g = nx.gaussian_random_partition_graph(n, variance, variance, p_in, p_out)
            is_connected = nx.is_connected(g)
        number = i * 7 + 1
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

        # 3. Returns the planted l-partition graph.
        k = rd.randint(2, 100)
        p_in = rd.uniform(0.3, 0.7)
        p_out = rd.uniform(0.05, 0.2)
        g = nx.planted_partition_graph(2, k, p_in, p_out)
        is_connected = nx.is_connected(g)
        while not is_connected:
            p_in = rd.uniform(0.3, 0.7)
            p_out = rd.uniform(0.05, 0.2)
            g = nx.planted_partition_graph(2, k, p_in, p_out)
            is_connected = nx.is_connected(g)
        number = i * 7 + 2
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

        # 4. Returns the random partition graph with a partition of sizes
        n1 = rd.randint(2, 100)
        n2 = rd.randint(2, (200-n1))
        p_in = rd.uniform(0.15, 0.35)
        p_out = rd.uniform(0.005, 0.02)
        g = nx.random_partition_graph([n1, n2], p_in, p_out)
        is_connected = nx.is_connected(g)
        while not is_connected:
            p_in = rd.uniform(0.15, 0.35)
            p_out = rd.uniform(0.005, 0.02)
            g = nx.random_partition_graph([n1, n2], p_in, p_out)
            is_connected = nx.is_connected(g)
        number = i * 7 + 3
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

        # 5. A ring of cliques graph is consisting of cliques, connected through single links.
        clique_size = rd.randint(2, 100)
        g = nx.ring_of_cliques(2, clique_size)
        number = i * 7 + 4
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

        # 6. Returns a stochastic block model graph
        n1 = rd.randint(2, 100)
        n2 = rd.randint(2, (200 - n1))
        p00 = rd.uniform(0.2, 0.4)
        p = rd.uniform(0.01, 0.075)
        p11 = rd.uniform(0.2, 0.4)
        g = nx.stochastic_block_model([n1, n2], [[p00, p], [p, p11]])
        is_connected = nx.is_connected(g)
        while not is_connected:
            p00 = rd.uniform(0.2, 0.4)
            p = rd.uniform(0.01, 0.075)
            p11 = rd.uniform(0.2, 0.4)
            g = nx.stochastic_block_model([n1, n2], [[p00, p], [p, p11]])
            is_connected = nx.is_connected(g)
        number = i * 7 + 5
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

        # 7. Generate a windmill graph.
        n = rd.randint(2, 100)
        g = nx.windmill_graph(2, n)
        number = i * 7 + 6
        filename = "graphs/" + str(number) + ".edgelist"
        nx.write_edgelist(g, filename, data=False)
        write_graph_properties(g, number)

    print("End")


def nth_moment(graph, n):
    """
    This function is used to calculate the n_th moment.
    The first moment is also known as the average degree <k>. The second moment is also known as <k^2>.
    :param graph:
    :param n: which moment it should return
    :return: nth_moment of type float
    """
    summation = 0
    for node in graph.nodes:
        summation += graph.degree[node] ** n
    return summation / len(graph)


def average_excess_degree(graph):
    """
    This function returns the average excess degree <g>
    :param graph:
    :return: average excess degree of type float
    """
    average = 0
    dictionary = nx.average_degree_connectivity(graph)
    for key in dictionary:
        average += key * dictionary[key]
    return average


def write_graph_properties(g, i):
    # n: #nodes,m: #edges,⟨k⟩: average degree,first moment,second moment,third moment,C: global clustering coefficient,
    # <C>: average local clustering coefficient, <d>: average shortest path length, average excess degree, radius, dens
    line = str(g.number_of_nodes()) + "," + str(g.size()) + "," + str(nth_moment(g, 1)) + "," + str(nth_moment(g, 2))\
           + "," + str(nth_moment(g, 3)) + "," + str(nx.transitivity(g)) + "," + str(nx.average_clustering(g))\
           + "," + str(nx.diameter(g)) + "," + str(nx.average_shortest_path_length(g))\
           + "," + str(average_excess_degree(g)) + "," + str(nx.radius(g)) + "," + str(nx.density(g))

    filename = "graphs/" + str(i) + ".properties"

    with open(filename, 'w') as the_file:
        the_file.write(line)


main()
